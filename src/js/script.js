$(document).ready(function(){
  $(".owl-carousel").owlCarousel({
    items: 1,
    // loop: true,
    // autoplay: true,
    responsive: {
      0: {
        loop: false,
        autoplay: false
      },

      992: {
        loop: true,
        autoplay: true
      }
    }
  });
});
